$(document).ready(function(){
    var selected_index = 0;
    var list_of_item =  $("#accordion");
    var length_of_list = $(list_of_item).children().length;

    // Fungsi menaikan item accordion
    $(".arrow-up").click(function(e) {
        e.preventDefault();
        selected_index = $(this).parent().parent().parent().index();
        var this_item = $(list_of_item).children().eq(selected_index);
        if (selected_index > 0) {
            $(list_of_item).children().eq(selected_index-1).before(this_item);
        } else {
            $(list_of_item).children().eq(length_of_list-1).after(this_item);
        }
    });

    // Fungsi menurunkan accordion
    $(".arrow-down").click(function(e) {
        e.preventDefault();
        selected_index = $(this).parent().parent().parent().index();
        var this_item = $(list_of_item).children().eq(selected_index);
        if (selected_index < length_of_list-1) {
            $(list_of_item).children().eq(selected_index+1).after(this_item);
        } else {
            $(list_of_item).children().eq(0).before(this_item);
        }
    });

    // Fungsi untuk turn off dan turn on darkmode
    $(".toggle-button").click(function() {
        if ($(this).hasClass("aktif")) {
            $(this).removeClass("aktif");
            $("body").css("background-color", "white");
            $(".message").css("color", "#393e46");
            $(".about-me").css("color", "#393e46");
            $(".mode").css("color", "#393e46");
            $(".mode").text("Mode Terang");
            $(".faq-text").css("text-decoration-color", "#393e46");
        } else {
            $(this).addClass("aktif");
            $("body").css("background-color", "#222831");
            $(".message").css("color", "#eeeeee");
            $(".about-me").css("color", "#eeeeee");
            $(".mode").css("color", "#eeeeee");
            $(".mode").text("Mode Gelap");
            $(".faq-text").css("text-decoration-color", "#eeeeee");
            $(".item").hover(function(){
                $(this).css("background-color", "white");
            }, function(){
                $(this).css("background-color", "#eeeeee");
            });
        }
    });

    // Fungsi untuk membuat item accordion dragable
    $("#accordion").sortable();

    // Membuka dan menutup content accordion
    $('.question, .content').click(function() {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).parent().siblings().hide(1000);
        } else {
            $('.question').removeClass("active");
            $('.content').hide(1000);
            $(this).addClass("active");
            $(this).parent().siblings().show(1000);
        }
    })

    // Menampilkan efek ketikan
    var typed = new Typed(
        '.animate', {
            strings: [
                'Saya seorang developer.',
                'Kalian dapat membuka pertanyaan tentang saya.',
                'Kalian juga dapat menukar urutan box sesuka hati.',
            ],
            typeSpeed: 50,
            backSpeed: 10,
            loop: true,
        }
    )
});