from django.test import TestCase, Client

# Create your tests here.
class TestStory7(TestCase):
    def test_if_url_is_existing(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_if_html_is_true(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story-7.html')

    def test_if_content_html_is_exist(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')

        self.assertIn("FAQ", html_response)
        self.assertIn("Rutinitas apa yang dilakukan sekarang?", html_response)
        self.assertIn("Kesibukan apa saja yang pernah dijalani?", html_response)
        self.assertIn("Apa saja prestasi yang pernah saya raih?", html_response)
        self.assertIn("Apa funfact dari diri saya?", html_response)